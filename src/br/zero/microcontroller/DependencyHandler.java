package br.zero.microcontroller;

public interface DependencyHandler<T> {
    public T create();

    public void dispose(T dependency);

    public void handleException(T dependency, Exception e);
}
