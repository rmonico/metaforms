package br.zero.microcontroller;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MicroController {

    private static final String DEPENDENCE_HANDLER_CREATOR_METHOD_NAME = "create";

    private static class CommandData {
        private Object instance;
        private Method method;

        public CommandData(Object instance, Method method) {
            this.instance = instance;
            this.method = method;
        }

    }

    private final Map<String, CommandData> commands;
    private final List<Object> dependencies;
    private final List<DependencyHandler<?>> dependencyHandlers;
    private PrintStream logOutput;
    private String controllerName;

    private MicroController(Map<String, CommandData> commands, List<Object> dependencies, List<DependencyHandler<?>> dependencyHandlers) {
        this.commands = commands;
        this.dependencies = dependencies;
        this.dependencyHandlers = dependencyHandlers;

        dependencies.add(this);
    }

    public MicroController() {
        this(new HashMap<String, CommandData>(), new LinkedList<Object>(), new LinkedList<DependencyHandler<?>>());
    }

    public MicroController(MicroController anotherController, String controllerName) {
        this(new HashMap<>(anotherController.commands), new LinkedList<>(anotherController.dependencies), new LinkedList<>(anotherController.dependencyHandlers));
        logOutput = anotherController.logOutput;
        this.controllerName = controllerName;

        dependencies.remove(anotherController);
    }

    public void registerActionClass(Object actionClass) {
        for (Method method : actionClass.getClass().getMethods()) {
            Command command = method.getAnnotation(Command.class);

            if (command != null) {
                commands.put(command.value(), new CommandData(actionClass, method));
            }
        }
    }

    public void run(String command) {
        CommandData commandData = commands.get(command);

        if (commandData == null) {
            throw new RuntimeException(String.format("Command not found: %s", command));
        }

        try {
            Method method = commandData.method;

            Class<?>[] parameterTypes = method.getParameterTypes();

            if (parameterTypes.length > 0) {
                List<Object> arguments = new LinkedList<>();

                DependencyHandler<Object> resolvedDependencyHandler = null;
                Object resolvedDependency = null;

                for (Class<?> parameterType : parameterTypes) {
                    resolvedDependency = resolveDependencyByInstance(parameterType);
                    resolvedDependencyHandler = null;

                    if (resolvedDependency != null)
                        arguments.add(resolvedDependency);
                    else {
                        resolvedDependencyHandler = resolveDependencyByHandler(parameterType, resolvedDependency);

                        if (resolvedDependencyHandler != null) {
                            resolvedDependency = resolvedDependencyHandler.create();
                            arguments.add(resolvedDependency);
                        } else {
                            throw new RuntimeException(String.format("Unable to resolve dependency class: \"%s\"", parameterType.toString()));
                        }
                    }
                }

                try {
                    log(String.format("Calling %s", method.toString()));
                    method.invoke(commandData.instance, arguments.toArray(new Object[] {}));

                    if (resolvedDependencyHandler != null) {
                        resolvedDependencyHandler.dispose(resolvedDependency);
                    }
                } catch (InvocationTargetException e) {
                    if (resolvedDependencyHandler != null) {
                        resolvedDependencyHandler.handleException(resolvedDependency, e);
                    } else {
                        throw new RuntimeException(e);
                    }
                }
            } else {
                log(String.format("Calling %s", method.toString()));
                method.invoke(commandData.instance);
            }
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private DependencyHandler<Object> resolveDependencyByHandler(Class<?> parameterType, Object resolvedDependency) throws NoSuchMethodException {
        for (DependencyHandler<?> dependencyHandler : dependencyHandlers) {
            Class<?> dependencyHandlerClass = getDependencyHandlerClass(dependencyHandler);
            if (parameterType.equals(dependencyHandlerClass)) {
                return (DependencyHandler<Object>) dependencyHandler;
            }
        }
        return null;
    }

    private Object resolveDependencyByInstance(Class<?> parameterType) {
        for (Object dependency : dependencies) {
            if (parameterType.equals(dependency.getClass())) {
                return dependency;
            }
        }

        return null;
    }

    private Class<?> getDependencyHandlerClass(DependencyHandler<?> dependencyHandler) throws NoSuchMethodException, SecurityException {
        Method method = dependencyHandler.getClass().getMethod(DEPENDENCE_HANDLER_CREATOR_METHOD_NAME);

        return method.getReturnType();
    }

    public void registerDependency(Object dependency) {
        dependencies.add(dependency);
    }

    public void registerDependencyHandler(DependencyHandler<?> dependencyHandler) {
        dependencyHandlers.add(dependencyHandler);
    }

    private void log(String message) {
        if (logOutput == null) {
            return;
        }

        logOutput.println(String.format("[%s] %s", controllerName, message));
    }

    public void setLogOutput(String controllerName, PrintStream out) {
        this.controllerName = controllerName;
        this.logOutput = out;
    }

}
