package br.zero.microcontroller;

public class DependencyHandlerAdapter<T> implements DependencyHandler<T> {

    @Override
    public T create() {
        return null;
    }

    @Override
    public void dispose(T dependency) {

    }

    @Override
    public void handleException(T dependency, Exception e) {

    }

}
