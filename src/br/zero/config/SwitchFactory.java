package br.zero.config;

public class SwitchFactory {

    public Switch newStringSwitch(String key, String shortKey, String defaultValue) {
        return new Switch(key, shortKey, defaultValue, false);
    }

    public Switch newStringSwitch(String key, String shortKey) {
        return new Switch(key, shortKey, null, false);
    }

    public Switch newStringSwitch(String key) {
        return new Switch(key, null, null, false);
    }

    public Switch newFlagSwitch(String key, String shortKey) {
        return new Switch(key, shortKey, null, true);
    }

}
