package br.zero.config;

public class Switch {

    private String key;
    private String shortKey;
    private String defaultValue;
    private boolean isFlag;

    public Switch(String key, String shortKey, String defaultValue, boolean isFlag) {
        this.key = key;
        this.shortKey = shortKey == null ? "" : shortKey;
        this.defaultValue = defaultValue;
        this.isFlag = isFlag;
    }

    public String getKey() {
        return key;
    }

    public String getShortKey() {
        return shortKey;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public boolean isFlag() {
        return isFlag;
    }

}
