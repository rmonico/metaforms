package br.zero.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class Config {

    private final Switch[] cmdLineSwitchs;
    private final Properties keys;

    public Config() {
        keys = new Properties();

        List<Switch> switchs = new ArrayList<>();
        createSwitchs(switchs, new SwitchFactory());
        cmdLineSwitchs = switchs.toArray(new Switch[] {});
    }

    protected void createSwitchs(List<Switch> switchs, SwitchFactory f) {
    }

    public Properties keys() {
        return keys;
    }

    public String key(String value) {
        Object object = keys.get(value);

        if (object == null) {
            return null;
        } else {
            return object.toString();
        }
    }

    public void parseCommandLineArguments(String[] args) {
        // TODO Throw error on invalid args

        createDefaultKeys();

        List<String> errors = new LinkedList<>();

        for (int i = 0; i < args.length; i++) {
            String arg = args[i];

            for (Switch cmdLineSwitch : cmdLineSwitchs) {
                String key = "--" + cmdLineSwitch.getKey();

                String shortKey = "-" + cmdLineSwitch.getShortKey();

                if ((key.equals(arg)) || (shortKey.equals(arg))) {
                    String value = null;
                    if (cmdLineSwitch.isFlag()) {
                        value = "";
                    } else {
                        if (i == args.length - 1) {
                            errors.add("Argument %s should have a value!");
                        } else
                            value = args[i + 1];
                    }

                    if (value != null)
                        keys.put(cmdLineSwitch.getKey(), value);

                    break;
                }
            }
        }
    }

    private void createDefaultKeys() {
        for (Switch cmdLineSwitch : cmdLineSwitchs) {
            String defaultValue = cmdLineSwitch.getDefaultValue();

            if (defaultValue != null) {
                keys.put(cmdLineSwitch.getKey(), defaultValue);
            }
        }
    }

    public void addFromStream(InputStream stream) throws IOException {
        keys.load(stream);
    }

}
