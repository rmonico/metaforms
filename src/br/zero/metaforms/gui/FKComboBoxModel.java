package br.zero.metaforms.gui;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.FKValue;
import br.zero.metaforms.model.Table;

public class FKComboBoxModel implements ComboBoxModel<FKValue> {

    private List<ListDataListener> listeners;
    private int selectedItem;
    private ArrayList<FKValue> elements;

    public FKComboBoxModel(Column column, Integer initialValue) {
        listeners = new LinkedList<ListDataListener>();

        createElements(column.getFKTable());

        findSelectedItem(initialValue);
    }

    private void createElements(Table fkTable) {
        elements = new ArrayList<FKValue>();

        Column primaryKey = fkTable.getPrimaryKeyColumn();
        Column descriptionColumn = fkTable.getDescriptionColumn();

        for (Map<Column, Object> row : fkTable.getData()) {
            Integer value = (Integer) row.get(primaryKey);
            Object fkDescriptionValue = row.get(descriptionColumn);

            FKValue element = new FKValue(value, fkDescriptionValue);
            elements.add(element);
        }
    }

    private void findSelectedItem(Integer initialValue) {
        if (initialValue == null)
            selectedItem = -1;
        else {
            for (int i = 0; i < elements.size(); i++) {
                FKValue fkValue = elements.get(i);
                if (initialValue.equals(fkValue.getValue())) {
                    selectedItem = i;

                    break;
                }
            }
        }
    }

    @Override
    public FKValue getElementAt(int index) {
        return elements.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListDataListener(ListDataListener listener) {
        listeners.remove(listener);
    }

    @Override
    public int getSize() {
        return elements.size();
    }

    @Override
    public Object getSelectedItem() {
        if (selectedItem == -1) {
            return null;
        }

        return elements.get(selectedItem);
    }

    @Override
    public void setSelectedItem(Object item) {
        selectedItem = elements.indexOf(item);
    }

}
