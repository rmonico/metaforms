package br.zero.metaforms.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.FKValue;
import br.zero.metaforms.model.MetaformsConfig;
import br.zero.metaforms.model.Table;
import br.zero.microcontroller.MicroController;

public class RowEditorBuilder {

    private RowEditor editor;
    private Table table;
    private RowValues data;
    private boolean isNewRow;
    private MicroController rowEditorController;
    private ActionListener rowEditorCommandListener;
    private MetaformsConfig config;

    public RowEditor create(MicroController masterController, MetaformsConfig config, Table table, Map<Column, Object> row, boolean isNewRow) {
        this.config = config;
        this.table = table;
        this.data = new RowValues(row);
        this.isNewRow = isNewRow;

        createRowEditor();

        createRowEditorController(masterController);
        rowEditorCommandListener = new CommandListener(rowEditorController);

        createContentPane();

        createColumnsPane();

        createButtonsPane();

        return editor;
    }

    private void createRowEditorController(MicroController masterController) {
        rowEditorController = new MicroController(masterController, "row editor");

        rowEditorController.registerDependency(table);
        rowEditorController.registerDependency(data);
        rowEditorController.registerActionClass(new RowEditorActions(editor));
    }

    private void createRowEditor() {
        editor = new RowEditor();

        String title = String.format("Row editor - %s", table.getName() + (isNewRow ? " (new)" : ""));

        editor.setTitle(title);
        editor.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private void createContentPane() {
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridBagLayout());
        editor.setContentPane(contentPane);
    }

    private void createColumnsPane() {
        JPanel columnsPane = new JPanel();
        columnsPane.setLayout(new GridBagLayout());

        createColumnControls(columnsPane);

        GridBagConstraints c = GBCFactory.create().gridy(0).allInsets(2).fill(GridBagConstraints.BOTH).done();
        editor.add(columnsPane, c);
    }

    private void createColumnControls(JPanel columnsPane) {
        int gridRow = 0;

        for (Column column : table.getColumns()) {
            Object value = data.getOldValues().get(column);

            createColumnLabel(columnsPane, gridRow, column);

            List<JComponent> controls = new LinkedList<>();

            JComponent inputControl = createColumnInputControl(columnsPane, column, gridRow, value);
            controls.add(inputControl);

            JCheckBox nullCheckbox = createColumnNullCheckbox(columnsPane, gridRow, value);
            controls.add(nullCheckbox);

            data.getControls().put(column, controls);

            gridRow++;
        }
    }

    private void createColumnLabel(JPanel columnsPane, int gridRow, Column column) {
        JLabel label = new JLabel(column.getName());
        GridBagConstraints c = GBCFactory.create().gridx(0).gridy(gridRow).anchor(GridBagConstraints.LINE_END).allInsets(2).done();
        columnsPane.add(label, c);
    }

    private JComponent createColumnInputControl(JPanel columnsPane, Column column, int gridRow, Object value) {
        JComponent control;

        if (column.getFKTable() != null) {
            JComboBox<FKValue> combobox = new JComboBox<>();

            int idValue = value != null ? ((FKValue) value).getValue() : -1;

            ComboBoxModel<FKValue> model = new FKComboBoxModel(column, idValue);

            combobox.setModel(model);

            control = combobox;
        } else {
            if (config.useTextArea(column)) {
                JTextArea textArea = new JTextArea();

                JScrollPane pane = new JScrollPane(textArea);

                textArea.setText(config.getInitialValue(column, value));

                pane.setPreferredSize(new Dimension(400, 400));
                GridBagConstraints c = GBCFactory.create().gridx(1).gridy(gridRow).allInsets(2).done();
                columnsPane.add(pane, c);

                return textArea;
            } else {
                JTextField text = new JTextField();

                text.setText(config.getInitialValue(column, value));

                control = text;
            }
        }

        control.setPreferredSize(new Dimension(150, control.getPreferredSize().height));
        GridBagConstraints c = GBCFactory.create().gridx(1).gridy(gridRow).allInsets(2).done();
        columnsPane.add(control, c);

        return control;
    }

    private JCheckBox createColumnNullCheckbox(JPanel columnsPane, int gridRow, Object value) {
        JCheckBox nullCheckBox = new JCheckBox("null?");
        nullCheckBox.setSelected(value == null);
        GridBagConstraints c = GBCFactory.create().gridx(2).gridy(gridRow).allInsets(2).done();
        columnsPane.add(nullCheckBox, c);

        return nullCheckBox;
    }

    private void createButtonsPane() {
        JPanel buttonsPane = new JPanel();
        buttonsPane.setLayout(new GridBagLayout());

        createButtons(buttonsPane);

        GridBagConstraints c = GBCFactory.create().anchor(GridBagConstraints.LINE_END).gridy(1).allInsets(2).done();
        editor.add(buttonsPane, c);
    }

    private void createButtons(JPanel buttonsPane) {
        GridBagConstraints c = GBCFactory.create().anchor(GridBagConstraints.LINE_END).allInsets(2).done();

        ButtonFactory factory = new ButtonFactory();

        factory.setPane(buttonsPane);
        factory.setConstraints(c);
        factory.listener(rowEditorCommandListener);

        JButton cancelButton = factory.create("Cancel").command("Close row editor").done();

        String okButtonLabel = isNewRow ? "Insert" : "Update";
        String okButtonCommand = isNewRow ? "Confirm insert" : "Confirm update";
        JButton okButton = factory.create(okButtonLabel).command(okButtonCommand).makeDefault(editor).done();

        makeButtonsHaveSameWidth(cancelButton, okButton);

    }

    private void makeButtonsHaveSameWidth(JButton cancelButton, JButton okButton) {
        Dimension cancelButtonSize = cancelButton.getPreferredSize();
        Dimension okButtonSize = okButton.getPreferredSize();
        if (cancelButtonSize.width > okButtonSize.width) {
            okButton.setPreferredSize(new Dimension(cancelButtonSize.width, okButtonSize.height));
        } else {
            cancelButton.setPreferredSize(new Dimension(okButtonSize.width, cancelButtonSize.height));
        }
    }
}
