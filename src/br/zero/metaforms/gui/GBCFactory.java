package br.zero.metaforms.gui;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public class GBCFactory {

    private GridBagConstraints gbc;

    public static GBCFactory create() {
        GBCFactory instance = new GBCFactory();

        instance.gbc = new GridBagConstraints();

        return instance;
    }

    public GBCFactory gridx(int gridx) {
        gbc.gridx = gridx;

        return this;
    }

    public GBCFactory gridy(int gridy) {
        gbc.gridy = gridy;

        return this;
    }

    public GBCFactory anchor(int anchor) {
        gbc.anchor = anchor;

        return this;
    }

    public GBCFactory allInsets(int allInsets) {
        gbc.insets = new Insets(allInsets, allInsets, allInsets, allInsets);

        return this;
    }

    public GridBagConstraints done() {
        return gbc;
    }

    public GBCFactory fill(int fill) {
        gbc.fill = fill;

        return this;
    }

}
