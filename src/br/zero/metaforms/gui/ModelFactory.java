package br.zero.metaforms.gui;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.ColumnType;
import br.zero.metaforms.model.MetaformsConfig;
import br.zero.metaforms.model.Model;
import br.zero.metaforms.model.Table;
import br.zero.microcontroller.Command;
import br.zero.microcontroller.MicroController;

public class ModelFactory {

    private MetaformsConfig config;
    private DatabaseMetaData metadata;
    private Model model;

    @Command("Initialize model")
    public void initializeModel(MetaformsConfig config, Connection connection, MicroController controller) throws SQLException, ClassNotFoundException {
        this.config = config;

        getJdbcMetadata(connection);

        populateModel();

        controller.registerDependency(model);

    }

    private void getJdbcMetadata(Connection connection) throws ClassNotFoundException, SQLException {
        Class.forName(config.getJdbcDriverClass());

        metadata = connection.getMetaData();
    }

    private void populateModel() throws SQLException {
        ResultSet tables = metadata.getTables(config.getCatalog(), config.getSchema(), config.getTableNamePattern(), config.getTableTypes());

        model = new Model();

        while (tables.next()) {
            String tableName = tables.getString("TABLE_NAME");
            Table table = populateTable(tableName);

            model.getTables().add(table);
        }

        tables.close();

        populateAllForeignKeys();
    }

    private Table populateTable(String tableName) throws SQLException {
        Table table = new Table();

        table.setName(tableName);

        populateColumns(table);

        populatePrimaryKeys(table);

        table.setDescriptionColumn(getDescriptionColumn(table));

        return table;
    }

    private void populateColumns(Table table) throws SQLException {
        ResultSet columns = metadata.getColumns(config.getCatalog(), config.getSchema(), table.getName(), "%");

        while (columns.next()) {
            Column column = new Column(table);

            column.setName(columns.getString("COLUMN_NAME"));
            column.setType(parseColumnType(columns.getString("TYPE_NAME")));

            table.getColumns().add(column);
        }

        columns.close();
    }

    private ColumnType parseColumnType(String columnType) {
        for (ColumnType type : ColumnType.values()) {
            if (type.getDatabaseTypeName().equals(columnType)) {
                return type;
            }
        }

        if (columnType == null) {
            return ColumnType.NO_TYPE;
        } else {
            return ColumnType.UNKNOWN_TYPE;
        }
    }

    private void populatePrimaryKeys(Table table) throws SQLException {
        ResultSet resultSet = metadata.getPrimaryKeys(config.getCatalog(), config.getSchema(), table.getName());

        while (resultSet.next()) {
            String columnName = resultSet.getString("COLUMN_NAME");

            Column column = table.getColumnByName(columnName);
            column.setPrimaryKey();
        }

        resultSet.close();
    }

    private Column getDescriptionColumn(Table table) {
        String key = String.format("tables.%s.description_column", table.getName());
        String descriptionColumnName = config.key(key);

        if (descriptionColumnName != null) {
            Column column = table.getColumnByName(descriptionColumnName);

            if (column != null) {
                return column;
            } else {
                System.err.println(String.format("Warning: Column specified by key \"%s\" not found on database...", key));
            }
        }

        String descriptionColumns = config.key("global.default_description_columns");

        if (descriptionColumns != null) {
            for (String columnName : descriptionColumns.split(",")) {
                Column column = table.getColumnByName(columnName);

                if (column != null) {
                    return column;
                }
            }
        }

        for (Column column : table.getColumns()) {
            if (ColumnType.TEXT.equals(column.getType())) {
                return column;
            }
        }

        return table.getPrimaryKeyColumn();
    }

    private void populateAllForeignKeys() throws SQLException {
        for (Table table : model.getTables()) {
            populateForeignKeys(table);
        }
    }

    private void populateForeignKeys(Table table) throws SQLException {
        ResultSet resultSet = metadata.getImportedKeys(config.getCatalog(), config.getSchema(), table.getName());

        while (resultSet.next()) {
            String columnName = resultSet.getString("FKCOLUMN_NAME");
            Column column = table.getColumnByName(columnName);

            String tableName = resultSet.getString("PKTABLE_NAME");
            Table fkTable = model.getTableByName(tableName);

            // TODO Not supporting compound PKs/FKs yet
            column.setFKTable(fkTable);
        }

        resultSet.close();
    }

}
