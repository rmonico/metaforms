package br.zero.metaforms.gui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import br.zero.metaforms.StringUtils;
import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.FKValue;
import br.zero.metaforms.model.MetaformsConfig;
import br.zero.metaforms.model.Model;
import br.zero.metaforms.model.Table;
import br.zero.microcontroller.Command;

public class DataTableActions {

    private Table table;
    private MetaformsConfig config;

    @Command("Table selected")
    public void tableSelected(MainWindow mainWindow, Model model, Connection connection, MetaformsConfig config) throws SQLException {
        this.config = config;
        model.setSelectedTableIndex(mainWindow.tableList.getSelectedIndex());

        table = model.getSelectedTable();

        updateModelData(connection);

        mainWindow.dataTableModel.fireTableStructureChanged();
    }

    private void updateModelData(Connection connection) throws SQLException {
        // FIX Create a data loaded flag instead create all this garbage...
        table.clearData();

        PreparedStatement stmt = connection.prepareStatement(getSelectStatement());
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Map<Column, Object> row = table.createRow();

            for (Column column : table.getColumns()) {
                row.put(column, getData(rs, column));
            }

            table.addRow(row);
        }

        rs.close();

        stmt.close();
    }

    private String getSelectStatement() {
        // = String.format("select * from %s;", table.getName());

        StringBuilder sql = new StringBuilder();

        sql.append("select\n");

        buildFieldList(sql);

        StringUtils.removeFromEnd(sql, 2);

        buildFromClause(sql);

        if (config.isDebugging()) {
            System.out.println(String.format("Running: %s", sql.toString()));
        }

        return sql.toString();
    }

    private void buildFieldList(StringBuilder sql) {
        for (Column column : table.getColumns()) {
            sql.append(String.format("  %s.%s as %2$s,\n", table.getName(), column.getName()));

            Table fkTable = column.getFKTable();
            if (fkTable != null) {
                 Column descriptionColumn = fkTable.getDescriptionColumn();
                // sql.append(String.format("  %s.%s as %1$s_%2$s,\n",
                // fkTable.getName(), descriptionColumn.getName()));
                String fkDescriptionColumnName = getFkDescriptionColumnName(fkTable);
                String descriptionColumnName = descriptionColumn.getName();
                sql.append(String.format("  %s.%s as %s,\n", fkTable.getName(), descriptionColumnName, fkDescriptionColumnName));
            }
        }
    }

    private void buildFromClause(StringBuilder sql) {
        sql.append("\n");

        sql.append("from\n");
        sql.append(String.format("  %s\n", table.getName()));

        List<Column> columns = table.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);

            Table fkTable = column.getFKTable();

            if (fkTable == null) {
                continue;
            }

            String fkTableName = fkTable.getName();
            sql.append(String.format("  left join %s\n", fkTableName));
            String columnName = column.getName();
            String fkColumnName = fkTable.getPrimaryKeyColumn().getName();
            sql.append(String.format("  on (%s.%s=%s.%s)", table.getName(), columnName, fkTableName, fkColumnName));

            if (i == columns.size() - 1) {
                sql.append(";");
            } else {
                sql.append("\n\n");
            }
        }
    }

    private Object getData(ResultSet rs, Column column) throws SQLException {
        String columnName = column.getName();

        switch (column.getType()) {
        case INTEGER:
            int value = rs.getInt(columnName);
            Table fkTable = column.getFKTable();
            if (fkTable == null) {
                return value;
            } else {
                String fkDescriptionColumnName = getFkDescriptionColumnName(fkTable);
                Object fkDescriptionValue = rs.getObject(fkDescriptionColumnName);
                return new FKValue(value, fkDescriptionValue);
            }
        case TEXT:
            return rs.getString(columnName);

        default:
            return rs.getObject(columnName);
        }
    }

    private String getFkDescriptionColumnName(Table fkTable) {
        Column descriptionColumn = fkTable.getDescriptionColumn();

        return String.format("%s_%s", fkTable.getName(), descriptionColumn.getName());
    }

}
