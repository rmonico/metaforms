package br.zero.metaforms.gui;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import br.zero.metaforms.StringUtils;
import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.FKValue;
import br.zero.metaforms.model.MetaformsConfig;
import br.zero.metaforms.model.Table;
import br.zero.microcontroller.Command;

public class RowEditorActions {

    private static final String SEPARATOR = ", ";
    private RowEditor editor;
    private RowValues values;
    private Connection connection;
    private MetaformsConfig config;

    public RowEditorActions(RowEditor editor) {
        this.editor = editor;
    }

    @Command("Close row editor")
    public void closeRowEditor() {
        editor.dispose();
    }

    @Command("Confirm insert")
    public void confirmInsert(Table table, MetaformsConfig config, RowValues values, Connection connection) throws SQLException {
        this.config = config;
        this.values = values;
        this.connection = connection;

        updateNewValuesMap();

        String fields = createFieldList();
        String valuesStr = createValueList();
        String sql = String.format("insert into %s (%s) values (%s);", table.getName(), fields, valuesStr);

        runStatement(sql);

        closeRowEditor();
    }

    private String createFieldList() {
        StringBuilder fieldList = new StringBuilder();

        for (Column column : values.getNewValues().keySet()) {
            fieldList.append(column.getName());
            fieldList.append(SEPARATOR);
        }

        StringUtils.removeFromEnd(fieldList, SEPARATOR.length());

        return fieldList.toString();
    }

    private String createValueList() {
        StringBuilder valueList = new StringBuilder();

        Map<Column, Object> newValues = values.getNewValues();

        for (Column column : newValues.keySet()) {
            // FIXME Não sei que tipo de valor é este, pode ser qualquer coisa!
            String value = newValues.get(column).toString();
            valueList.append(value);

            valueList.append(SEPARATOR);
        }

        StringUtils.removeFromEnd(valueList, SEPARATOR.length());

        return valueList.toString();
    }

    @Command("Confirm update")
    public void confirmUpdate(Table table, MetaformsConfig config, RowValues values, Connection connection) throws SQLException {
        this.config = config;
        this.values = values;
        this.connection = connection;

        updateNewValuesMap();

        String valuesStr = createValues();
        String pkCondition = createPkCondition();

        String sql = String.format("update %s set %s where %s;", table.getName(), valuesStr, pkCondition);

        runStatement(sql);

        closeRowEditor();
    }

    private void runStatement(String sql) throws SQLException {
        if (config.isDebugging()) {
            System.out.println(String.format("Running: %s", sql));
        }

        PreparedStatement prepareStatement = connection.prepareStatement(sql);

        prepareStatement.executeUpdate();
    }

    private void updateNewValuesMap() {
        // FIXME Its coupled to UI controls, should be done in
        // RowEditorBuilder.okButton!

        Map<Column, List<JComponent>> controlMap = values.getControls();
        for (Column column : controlMap.keySet()) {
            List<JComponent> controls = controlMap.get(column);

            Object value = getValueFromUI(column, controls);
            JCheckBox checkbox = (JCheckBox) controls.get(1);

            if (!checkbox.isSelected()) {
                // FIXME Grave: Não vai lançar null em um campo que tiver outro
                // valor!
                values.getNewValues().put(column, parseValue(column, value));
            }
        }

    }

    private Object getValueFromUI(Column column, List<JComponent> controls) {
        if (column.getFKTable() != null) {
            @SuppressWarnings("unchecked")
            JComboBox<String> combobox = (JComboBox<String>) controls.get(0);

            return combobox.getSelectedItem();
        } else {
            JTextComponent text;

            if (config.useTextArea(column)) {
                text = (JTextArea) controls.get(0);
            } else {
                text = (JTextField) controls.get(0);
            }

            Object value = text.getText();

            return value;
        }
    }

    private Object parseValue(Column column, Object value) {
        switch (column.getType()) {
        case INTEGER: {
            if (column.getFKTable() != null) {
                FKValue fkValue = (FKValue) value;
                return fkValue.getValue();
            } else {
                return Integer.parseInt(value.toString());
            }
        }

        case TEXT: {
            // FIXME Grave: Scape this text.getText before return!
            return String.format("\"%s\"", value.toString());
        }
        default:
            return null;
        }
    }

    private String createValues() {
        StringBuilder valuesStr = new StringBuilder();

        Map<Column, Object> newValues = values.getNewValues();

        for (Column column : newValues.keySet()) {
            // FIXME Não sei que tipo de valor é este, pode ser qualquer coisa!
            String value = newValues.get(column).toString();

            valuesStr.append(String.format("%s=%s", column.getName(), value));
            valuesStr.append(SEPARATOR);
        }

        StringUtils.removeFromEnd(valuesStr, SEPARATOR.length());

        return valuesStr.toString();
    }

    private String createPkCondition() {
        StringBuilder condition = new StringBuilder();

        Map<Column, Object> oldValues = values.getOldValues();
        for (Column column : oldValues.keySet()) {
            if (!column.isPrimaryKey()) {
                continue;
            }

            String value = oldValues.get(column).toString();

            condition.append(String.format("%s=%s, ", column.getName(), value));
        }

        condition.delete(condition.length() - 2, condition.length());

        return condition.toString();
    }
}
