package br.zero.metaforms.gui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;

import br.zero.metaforms.model.Column;

public class RowValues {

    private Map<Column, Object> oldValues;
    private Map<Column, Object> newValues;
    private Map<Column, List<JComponent>> controls;

    public RowValues(Map<Column, Object> oldValues) {
        this.oldValues = oldValues;
        newValues = new HashMap<>();
        controls = new HashMap<>();
    }

    public Map<Column, Object> getOldValues() {
        return oldValues;
    }

    public Map<Column, Object> getNewValues() {
        return newValues;
    }

    public Map<Column, List<JComponent>> getControls() {
        return controls;
    }

}
