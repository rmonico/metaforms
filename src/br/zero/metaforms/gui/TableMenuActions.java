package br.zero.metaforms.gui;

import java.util.Map;

import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.MetaformsConfig;
import br.zero.metaforms.model.Model;
import br.zero.metaforms.model.Table;
import br.zero.microcontroller.Command;
import br.zero.microcontroller.MicroController;

public class TableMenuActions {

    @Command("New record")
    public void newRecord(MicroController controller, MetaformsConfig config, Model model) {
        createEditor(controller, config, model, true);
    }

    @Command("Update record")
    public void updateRecord(MicroController controller, MetaformsConfig config, Model model) {
        createEditor(controller, config, model, false);
    }

    private void createEditor(MicroController controller, MetaformsConfig config, Model model, boolean isNewRow) {
        Table table = model.getSelectedTable();
        Map<Column, Object> row = isNewRow ? table.createRow() : table.getSelectedRow();

        RowEditorBuilder factory = new RowEditorBuilder();

        RowEditor editor = factory.create(controller, config, table, row, isNewRow);

        editor.run();

        // TODO Atualizar dataTable de volta...
    }

    @Command("Delete record")
    public void deleteRecord() {

    }

    @Command("Refresh table")
    public void refreshTable() {

    }

}
