package br.zero.metaforms.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import br.zero.microcontroller.MicroController;

public class CommandListener implements ActionListener {
    private final MicroController controller;

    public CommandListener(MicroController controller) {
        this.controller = controller;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String command = event.getActionCommand();

        controller.run(command);
    }
}