package br.zero.metaforms.gui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ButtonFactory {

    private JButton button;
    private JPanel pane;
    private GridBagConstraints constraints;
    private ActionListener listener;

    public ButtonFactory create(String label) {
        button = new JButton(label);

        return this;
    }

    public ButtonFactory setPane(JPanel pane) {
        this.pane = pane;
        return this;
    }

    public ButtonFactory command(String command) {
        button.setActionCommand(command);
        return this;
    }

    public ButtonFactory setConstraints(GridBagConstraints c) {
        this.constraints = c;

        return this;
    }

    public ButtonFactory makeDefault(JFrame frame) {
        frame.getRootPane().setDefaultButton(button);

        return this;
    }

    public ButtonFactory listener(ActionListener listener) {
        this.listener = listener;

        return this;
    }

    public JButton done() {
        pane.add(button, constraints);

        button.addActionListener(listener);

        return button;
    }

}
