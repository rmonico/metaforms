package br.zero.metaforms.gui;

import javax.swing.JOptionPane;

import br.zero.metaforms.model.MetaformsConfig;
import br.zero.microcontroller.Command;

public class FileMenuActions {

    @Command("Exit")
    public void exit(MainWindow mainWindow, MetaformsConfig config) {
        if (config.isDebugging()) {
            mainWindow.dispose();

            return;
        }

        int result = JOptionPane.showConfirmDialog(mainWindow, "Tem certeza de que deseja sair?", "Metaforms", JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.YES_OPTION) {
            mainWindow.dispose();
        }
    }

    @Command("Open file")
    public void open(MainWindow mainWindow) {
        JOptionPane.showMessageDialog(mainWindow, "Not implemented yet...", "Metaforms", JOptionPane.WARNING_MESSAGE);
    }

}
