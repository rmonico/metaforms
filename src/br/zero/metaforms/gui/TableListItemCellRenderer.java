package br.zero.metaforms.gui;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import br.zero.metaforms.model.Table;

public class TableListItemCellRenderer extends DefaultListCellRenderer {

    /**
	 * 
	 */
    private static final long serialVersionUID = 2476055958453764916L;

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        String text = ((Table) value).getName();
        return super.getListCellRendererComponent(list, text, index, isSelected, cellHasFocus);
    }
}
