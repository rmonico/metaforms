package br.zero.metaforms.gui;

import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MenuItemFactory {

    private JMenuItem menuItem;
    private JMenu menu;
    private ActionListener listener;

    public MenuItemFactory create() {
        menuItem = new JMenuItem();

        return this;
    }

    public MenuItemFactory setMenu(JMenu menu) {
        this.menu = menu;

        return this;
    }

    public MenuItemFactory setup(String label, String command, int mnemonic, KeyStroke keyStroke) {
        menuItem.setText(label);

        menuItem.setMnemonic(mnemonic);
        menuItem.setAccelerator(keyStroke);

        menuItem.setActionCommand(command);

        return this;
    }

    public MenuItemFactory listener(ActionListener listener) {
        this.listener = listener;

        return this;
    }

    public JMenuItem done() {
        menuItem.addActionListener(listener);

        menu.add(menuItem);

        return menuItem;
    }

}
