package br.zero.metaforms.gui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

import br.zero.metaforms.model.MetaformsConfig;
import br.zero.metaforms.model.Model;
import br.zero.metaforms.model.Table;
import br.zero.microcontroller.Command;
import br.zero.microcontroller.MicroController;

public class MainWindowBuilder {

    private MainWindow mainWindow;
    private MicroController controller;
    private CommandListener mainWindowListener;
    private Model model;
    private MetaformsConfig config;

    @Command("Initialize main window")
    public void initializeMainWindow(MicroController bootController, MetaformsConfig config, Model model) {
        this.config = config;
        this.model = model;
        controller = new MicroController(bootController, "main");

        controller.registerActionClass(new FileMenuActions());
        controller.registerActionClass(new DebugMenuActions());
        controller.registerActionClass(new DataTableActions());
        controller.registerActionClass(new TableMenuActions());

        JFrame.setDefaultLookAndFeelDecorated(true);

        mainWindowListener = new CommandListener(controller);

        createMainWindow();

        createContentPane();

        createTableList();

        createDataTable();

        populateDataTable();

        createMainMenu();

        mainWindow.run();
    }

    private void createMainWindow() {
        mainWindow = new MainWindow();

        mainWindow.setTitle(String.format("Meta Forms - %s", config.getConnectionString()));
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        controller.registerDependency(mainWindow);
    }

    private void createContentPane() {
        JPanel contentPane = new JPanel();

        contentPane.setLayout(new GridBagLayout());
        mainWindow.setContentPane(contentPane);
    }

    private JComponent createTableList() {
        JList<Table> list = new JList<>();

        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setModel(new TableListModel(model.getTables()));
        list.setCellRenderer((ListCellRenderer<? super Table>) new TableListItemCellRenderer());
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(new TableListSelectionListener(controller, config));

        JScrollPane jScrollPane = new JScrollPane(list);

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.VERTICAL;
        c.weighty = 1;
        c.weightx = 0;
        c.anchor = GridBagConstraints.WEST;
        c.insets = new Insets(2, 2, 2, 2);
        mainWindow.getContentPane().add(jScrollPane, c);

        mainWindow.tableList = list;

        return jScrollPane;
    }

    private Component createDataTable() {
        JTable table = new JTable();

        ListSelectionModel selectionModel = table.getSelectionModel();

        selectionModel.addListSelectionListener(new DataTableListener(model));

        JScrollPane jScrollPane = new JScrollPane(table);

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1;
        c.weightx = 1;
        c.insets = new Insets(2, 2, 2, 2);
        mainWindow.getContentPane().add(jScrollPane, c);

        mainWindow.dataTable = table;

        return jScrollPane;
    }

    private void populateDataTable() {
        mainWindow.dataTableModel = new DataTableModel(model);

        mainWindow.dataTable.setModel(mainWindow.dataTableModel);
        mainWindow.tableList.setSelectedIndex(0);
    }

    private void createMainMenu() {
        JMenuBar mainMenu = new JMenuBar();

        mainMenu.add(createFileMenu());
        mainMenu.add(createTableMenu());

        if (config.isDebugging()) {
            mainMenu.add(createDebugMenu());
        }

        mainWindow.setJMenuBar(mainMenu);
    }

    private JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("File");

        fileMenu.setMnemonic(KeyEvent.VK_F);

        MenuItemFactory factory = new MenuItemFactory();

        factory.setMenu(fileMenu);
        factory.listener(mainWindowListener);

        factory.create().setup("Open", "Open file", KeyEvent.VK_O, KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK)).done();
        fileMenu.addSeparator();
        factory.create().setup("Exit", "Exit", KeyEvent.VK_E, KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK)).done();

        return fileMenu;
    }

    private JMenu createTableMenu() {
        JMenu tableMenu = new JMenu("Table");

        tableMenu.setMnemonic(KeyEvent.VK_T);

        MenuItemFactory factory = new MenuItemFactory();

        factory.setMenu(tableMenu);
        factory.listener(mainWindowListener);

        factory.create().setup("New record", "New record", KeyEvent.VK_N, KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK)).done();
        factory.create().setup("Update record", "Update record", KeyEvent.VK_U, KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK)).done();
        factory.create().setup("Delete record", "Delete record", KeyEvent.VK_D, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0)).done();
        factory.create().setup("Refresh table", "Refresh table", KeyEvent.VK_R, KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK)).done();

        return tableMenu;
    }

    private JMenu createDebugMenu() {
        JMenu debugMenu = new JMenu("DEBUG");

        MenuItemFactory factory = new MenuItemFactory();

        factory.listener(mainWindowListener);

        factory.setMenu(debugMenu);
        factory.create().setup("Recreate Screen", "Recreate Screen", KeyEvent.VK_R, KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK + ActionEvent.ALT_MASK)).done();
        factory.create().setup("Show model metadata", "Show model metadata", KeyEvent.VK_M, null).done();
        factory.create().setup("Show model data", "Show model data", KeyEvent.VK_S, null).done();

        return debugMenu;
    }

}
