package br.zero.metaforms.gui;

import java.util.List;

import javax.swing.AbstractListModel;

import br.zero.metaforms.model.Table;

public class TableListModel extends AbstractListModel<Table> {

    /**
	 * 
	 */
    private static final long serialVersionUID = 2707432381568198322L;
    private final List<Table> tables;

    public TableListModel(List<Table> tables) {
        this.tables = tables;
    }

    @Override
    public Table getElementAt(int index) {
        return tables.get(index);
    }

    @Override
    public int getSize() {
        return tables.size();
    }

}
