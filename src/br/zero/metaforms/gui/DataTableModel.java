package br.zero.metaforms.gui;

import javax.swing.table.AbstractTableModel;

import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.Model;
import br.zero.metaforms.model.Table;

public class DataTableModel extends AbstractTableModel {

    /**
	 * 
	 */
    private static final long serialVersionUID = -8864347291599849416L;

    private final Model model;

    public DataTableModel(Model model) {
        this.model = model;
    }

    @Override
    public int getColumnCount() {
        Table selectedTable = model.getSelectedTable();

        if (selectedTable == null) {
            return 0;
        }

        return selectedTable.getColumns().size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        Table table = model.getSelectedTable();

        if (table == null) {
            return "";
        }

        Column column = table.getColumns().get(columnIndex);
        String columnName = String.format("%s%s", getColumnPrefix(column), column.getName());

        return columnName;
    }

    private String getColumnPrefix(Column column) {
        StringBuilder prefix = new StringBuilder();

        if (column.isPrimaryKey()) {
            prefix.append("PK");
        }

        if (column.getFKTable() != null) {
            if (prefix.length() > 0)
                prefix.append(",");
            prefix.append("FK");
        }

        if (prefix.length() == 0)
            return "";
        else
            return String.format("[%s] ", prefix.toString());
    }

    @Override
    public int getRowCount() {
        Table table = model.getSelectedTable();

        if (table == null) {
            return 0;
        }

        return table.getRowCount();
    }

    @Override
    public Object getValueAt(int row, int columnIndex) {
        Table table = model.getSelectedTable();

        if (table == null) {
            return "";
        }

        Column column = table.getColumns().get(columnIndex);
        Object value = table.cell(column, row);

        if (value == null)
            return "<null>";
        else
            return value.toString();
    }

}
