package br.zero.metaforms.gui;

import javax.swing.JFrame;

public class RootFrame extends JFrame {

    /**
	 * 
	 */
    private static final long serialVersionUID = 3513414888365325204L;

    public void run() {
        pack();

        setVisible(true);
    }

}
