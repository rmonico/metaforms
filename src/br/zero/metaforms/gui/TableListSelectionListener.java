package br.zero.metaforms.gui;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import br.zero.metaforms.model.MetaformsConfig;
import br.zero.microcontroller.MicroController;

public class TableListSelectionListener implements ListSelectionListener {
    /**
	 * 
	 */
    private final MicroController controller;
    private MetaformsConfig config;

    TableListSelectionListener(MicroController controller, MetaformsConfig config) {
        this.controller = controller;
        this.config = config;
    }

    @Override
    public void valueChanged(ListSelectionEvent event) {
        if (event.getValueIsAdjusting()) {
            return;
        }

        controller.run("Table selected");

        if (config.isDebugging()) {
            JList<?> list = (JList<?>) event.getSource();
            System.out.println(String.format("Table selected: %d", list.getSelectedIndex()));
        }
    }
}