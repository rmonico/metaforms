package br.zero.metaforms.gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import br.zero.metaforms.model.MetaformsConfig;
import br.zero.microcontroller.DependencyHandler;

public class ConnectionHandler implements DependencyHandler<Connection> {

    private MetaformsConfig config;

    public ConnectionHandler(MetaformsConfig config) {
        this.config = config;
    }

    @Override
    public Connection create() {
        try {
            Connection connection = DriverManager.getConnection(config.getConnectionString());

            if (config.isDebugging()) {
                showTransactionIsolation(connection);
            }

            return connection;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void showTransactionIsolation(Connection connection) throws SQLException {
        int isolation = connection.getTransactionIsolation();

        String transactionIsolation;
        if (isolation == Connection.TRANSACTION_NONE) {
            transactionIsolation = "TRANSACTION_NONE";
        } else if (isolation == Connection.TRANSACTION_READ_COMMITTED) {
            transactionIsolation = "TRANSACTION_READ_COMMITTED";
        } else if (isolation == Connection.TRANSACTION_READ_UNCOMMITTED) {
            transactionIsolation = "TRANSACTION_READ_UNCOMMITTED";
        } else if (isolation == Connection.TRANSACTION_REPEATABLE_READ) {
            transactionIsolation = "TRANSACTION_REPEATABLE_READ";
        } else if (isolation == Connection.TRANSACTION_SERIALIZABLE) {
            transactionIsolation = "TRANSACTION_SERIALIZABLE";
        } else {
            transactionIsolation = "<Unknown transaction isolation flag...>";
        }

        System.out.println(String.format("Transaction isolation: %s", transactionIsolation));
    }

    @Override
    public void dispose(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void handleException(Connection connection, Exception e) {
        try {
            connection.close();
        } catch (SQLException e1) {
            System.err.println();
            System.err.println("*** Handler exception ***");
            e1.printStackTrace();
            System.err.println("*** End handler exception ***");
            System.err.println();
        }

        System.err.println("*** Original exception ***");
        throw new RuntimeException(e);
    }
}
