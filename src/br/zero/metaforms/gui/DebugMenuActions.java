package br.zero.metaforms.gui;

import java.util.List;

import br.zero.metaforms.Main;
import br.zero.metaforms.model.Column;
import br.zero.metaforms.model.ColumnType;
import br.zero.metaforms.model.Model;
import br.zero.metaforms.model.Table;
import br.zero.microcontroller.Command;

public class DebugMenuActions {

    @Command("Recreate Screen")
    public void recreate_screen(MainWindow mainWindow) {
        // FIXME Não está liberando os roweditors!
        mainWindow.dispose();

        Main.main(null);
    }

    @Command("Show model metadata")
    public void showModel(Model model) {
        List<Table> tables = model.getTables();

        for (Table table : tables) {
            System.out.println(String.format("Name: %s", table.getName()));

            List<Column> columns = table.getColumns();

            for (Column column : columns) {
                String isPk = column.isPrimaryKey() ? "PK " : "";
                String name = String.format("%s%s", isPk, column.getName());
                ColumnType columnType = column.getType();
                String type = columnType == null ? "null" : columnType.toString();
                System.out.println(String.format("  %s: %s", name, type));
            }
        }

        Table selectedTable = model.getSelectedTable();

        if (selectedTable != null) {
            System.out.println(String.format("Selected table: %s", selectedTable.getName()));
        }
    }

    @Command("Show model data")
    public void showTableData(Model model) {
        List<Table> tables = model.getTables();

        for (Table table : tables) {
            System.out.println(String.format("Name: %s", table.getName()));

            for (int i = 0; i < table.getRowCount(); i++) {
                StringBuilder row = new StringBuilder();

                for (Column column : table.getColumns()) {
                    Object value = table.cell(column, i);

                    String valueStr = value == null ? "<null>" : value.toString();

                    row.append(valueStr);
                    row.append(";");
                }

                System.out.println(row.toString());
            }
        }
    }
}
