package br.zero.metaforms.gui;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import br.zero.metaforms.model.MetaformsConfig;
import br.zero.microcontroller.Command;
import br.zero.microcontroller.MicroController;

public class ConfigFactory {

    MetaformsConfig config;

    @Command("Initialize config")
    public void initializeConfig(MicroController controller, String[] args) throws IOException {
        config = new MetaformsConfig();

        config.parseCommandLineArguments(args);

        String configFileName = config.getConfigFile();
        if (configFileName == null)
            throw new RuntimeException("--configfile must be specified!");

        InputStream stream = new FileInputStream(configFileName);
        config.addFromStream(stream);

        controller.registerDependency(config);
        controller.registerDependencyHandler(new ConnectionHandler(config));
    }

}
