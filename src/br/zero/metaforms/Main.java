package br.zero.metaforms;

import br.zero.metaforms.gui.ConfigFactory;
import br.zero.metaforms.gui.MainWindowBuilder;
import br.zero.metaforms.gui.ModelFactory;
import br.zero.microcontroller.MicroController;

public class Main {

    public static void main(final String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                Main.run(args);
            }

        });
    }

    private static void run(String[] args) {
        MicroController controller = new MicroController();

        controller.setLogOutput("BOOT", System.out);

        controller.registerActionClass(new ConfigFactory());
        controller.registerActionClass(new ModelFactory());
        controller.registerActionClass(new MainWindowBuilder());

        controller.registerDependency(args);

        controller.run("Initialize config");
        controller.run("Initialize model");
        controller.run("Initialize main window");
    }
}
