package br.zero.metaforms.model;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<Table> tables;
    private int selectedTableIndex;

    public Model() {
        tables = new ArrayList<>();
        selectedTableIndex = -1;
    }

    public List<Table> getTables() {
        return tables;
    }

    public void setSelectedTableIndex(int selectedTableIndex) {
        this.selectedTableIndex = selectedTableIndex;
    }

    public Table getSelectedTable() {
        if (selectedTableIndex == -1) {
            return null;
        } else {
            return tables.get(selectedTableIndex);
        }
    }

    public Table getTableByName(String tableName) {
        for (Table table : tables) {
            if (table.getName().equals(tableName)) {
                return table;
            }
        }

        return null;
    }

}
