package br.zero.metaforms.model;

public class Column {

    private Table table;
    private String name;
    private ColumnType type;
    private Table fkTable;

    public Column(Table table) {
        this.table = table;
    }

    public Table getTable() {
        return table;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ColumnType getType() {
        return type;
    }

    public void setType(ColumnType type) {
        this.type = type;
    }

    public void setPrimaryKey() {
        table.setPrimaryKeyColumn(this);
    }

    public boolean isPrimaryKey() {
        Column tablePrimaryKey = table.getPrimaryKeyColumn();

        if (tablePrimaryKey == null) {
            return false;
        }

        return name.equals(tablePrimaryKey.getName());
    }

    public void setFKTable(Table fkTable) {
        this.fkTable = fkTable;
    }

    public Table getFKTable() {
        return fkTable;
    }

}
