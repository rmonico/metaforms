package br.zero.metaforms.model;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import br.zero.config.Config;
import br.zero.config.Switch;
import br.zero.config.SwitchFactory;

public class MetaformsConfig extends Config {

    @Override
    protected void createSwitchs(List<Switch> switchs, SwitchFactory f) {
        switchs.add(f.newStringSwitch("configfile", "F"));
        switchs.add(f.newStringSwitch("jdbcdriver", "J", "org.sqlite.JDBC"));
        switchs.add(f.newStringSwitch("connectionstring", "C"));
        switchs.add(f.newStringSwitch("catalog"));
        switchs.add(f.newStringSwitch("schema"));
        switchs.add(f.newStringSwitch("tablenamepattern", null, "%"));
        switchs.add(f.newStringSwitch("tabletypes", null, "TABLE"));

        switchs.add(f.newFlagSwitch("debug", "D"));
        switchs.add(f.newFlagSwitch("help", "h"));
    }

    public String getConfigFile() {
        return key("configfile");
    }

    public String getJdbcDriverClass() {
        return key("jdbcdriver");
    }

    public String getConnectionString() {
        return key("connectionstring");
    }

    public String getCatalog() {
        return key("catalog");
    }

    public String getSchema() {
        return key("schema");
    }

    public String getTableNamePattern() {
        return key("tablenamepattern");
    }

    public String[] getTableTypes() {
        String tableTypes = key("tabletypes");

        return tableTypes.split(",");
    }

    public boolean isDebugging() {
        return key("debug") != null;
    }

    public boolean useTextArea(Column column) {
        String editor = key(getBaseKeyOfColumn(column, "editor"));

        return "textarea".equals(editor);
    }

    private String getBaseKeyOfColumn(Column column, String suffix) {
        Table table = column.getTable();

        String keyOfColumn = String.format("tables.%s.%s.%s", table.getName(), column.getName(), suffix);

        return keyOfColumn;
    }

    public String getInitialValue(Column column, Object value) {
        if (value == null) {
            String initialValue = key(getBaseKeyOfColumn(column, "initialvalue"));

            if (initialValue == null) {
                return "";
            } else {
                if ("#today".equals(initialValue)) {
                    return todayString();
                } else {
                    return initialValue;
                }
            }
        } else {
            return value.toString();
        }
    }

    private String todayString() {
        Locale locale = Locale.getDefault();

        return new SimpleDateFormat("dd/MM/yyyy", locale).format(new GregorianCalendar(locale).getTime());
    }

}
