package br.zero.metaforms.model;

public class FKValue {

    private int value;
    private Object fkDescriptionValue;

    public FKValue(int value, Object fkDescriptionValue) {
        this.value = value;
        this.fkDescriptionValue = fkDescriptionValue;
    }

    public int getValue() {
        return value;
    }

    public Object getFkDescriptionValue() {
        return fkDescriptionValue;
    }

    @Override
    public String toString() {
        if (fkDescriptionValue == null) {
            return "<null>";
        }

        String valueStr = Integer.toString(value);
        String descriptionStr = fkDescriptionValue.toString();

        return String.format("#%s: %s", valueStr, descriptionStr);
    }

}
