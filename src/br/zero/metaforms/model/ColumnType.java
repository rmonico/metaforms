package br.zero.metaforms.model;

public enum ColumnType {
    INTEGER("INTEGER"), TEXT("TEXT"), NO_TYPE("No type"), UNKNOWN_TYPE("Unknown type");

    private String databaseTypeName;

    private ColumnType(String databaseTypeName) {
        this.databaseTypeName = databaseTypeName;
    }

    public String getDatabaseTypeName() {
        return databaseTypeName;
    }

    @Override
    public String toString() {
        return databaseTypeName;
    }
}
