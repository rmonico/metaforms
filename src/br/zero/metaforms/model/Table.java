package br.zero.metaforms.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Table {

    private String name;
    private List<Column> columns;
    private List<Map<Column, Object>> data;
    private int selectedRowIndex;
    private Column primaryKeyColumn;
    private Column descriptionColumn;

    public Table() {
        columns = new LinkedList<>();
        data = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public Map<Column, Object> createRow() {
        return new HashMap<>();
    }

    public void addRow(Map<Column, Object> row) {
        // TODO Check if row has all columns in table
        data.add(row);
    }

    public int getRowCount() {
        return data.size();
    }

    public Object cell(Column column, int row) {
        return data.get(row).get(column);
    }

    public void setSelectedRowIndex(int selectedRowIndex) {
        this.selectedRowIndex = selectedRowIndex;
    }

    public Map<Column, Object> getSelectedRow() {
        return data.get(selectedRowIndex);
    }

    public void clearData() {
        data = new LinkedList<>();
    }

    public Column getColumnByName(String columnName) {
        for (Column column : columns) {
            if (column.getName().equals(columnName)) {
                return column;
            }
        }

        return null;
    }

    public void setDescriptionColumn(Column descriptionColumn) {
        this.descriptionColumn = descriptionColumn;
    }

    public Column getDescriptionColumn() {
        return descriptionColumn;
    }

    public void setPrimaryKeyColumn(Column primaryKeyColumn) {
        this.primaryKeyColumn = primaryKeyColumn;

    }

    public Column getPrimaryKeyColumn() {
        return primaryKeyColumn;
    }

    public List<Map<Column, Object>> getData() {
        return data;
    }
}
