package br.zero.microcontroller.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.zero.microcontroller.Command;
import br.zero.microcontroller.DependencyHandler;
import br.zero.microcontroller.MicroController;

public class MicroControllerTests {

    public static class ComplexObject {
        public final String value;

        public ComplexObject(String value) {
            this.value = value;
        }
    }

    public static class TestActionClass {

        public boolean ran = false;
        Dependency dependency;
        public MicroController controller;

        @Command("test")
        public void test() {
            ran = true;
        }

        @Command("test_injection_by_param")
        public void test_injection_by_param(ComplexObject complexObject) {
            ran = "Value of complex object".equals(complexObject.value);
        }

        @Command("test_handled_dependency")
        public void test_handled_dependency(Dependency dependency) {
            ran = dependency != null;
        }

        @Command("test_dispose_dependency")
        public void test_dispose_dependency(Dependency dependency) {
            this.dependency = dependency;
        }

        @Command("test_exception_handle")
        public void test_exception_handle(Dependency dependency) {
            this.dependency = dependency;
            throw new RuntimeException();
        }
        
        @Command("test_dependency_reuse")
        public void test_dependency_reuse(Dependency dependency, MicroController controller) {
            ran = true;
            this.dependency = dependency;
            this.controller = controller;
        }
    }

    @Test
    public void should_run_command_throught_controller() {
        MicroController controller = new MicroController();

        TestActionClass actionClass = new TestActionClass();
        controller.registerActionClass(actionClass);

        controller.run("test");

        assertTrue(actionClass.ran);
    }

    @Test
    public void should_inject_dependency_by_param() {
        MicroController controller = new MicroController();

        TestActionClass actionClass = new TestActionClass();
        controller.registerActionClass(actionClass);

        ComplexObject complexObject = new ComplexObject("Value of complex object");
        controller.registerDependency(complexObject);

        controller.run("test_injection_by_param");

        assertTrue(actionClass.ran);
    }

    public static class Dependency {
        public boolean disposed = false;
        public boolean exceptionHandled;
    }

    public static class DependencyHandlerImpl implements DependencyHandler<Dependency> {
        public Dependency create() {
            return new Dependency();
        }

        @Override
        public void dispose(Dependency dependency) {
            dependency.disposed = true;
        }

        @Override
        public void handleException(Dependency dependency, Exception e) {
            dependency.exceptionHandled = e != null;
        }
    }

    @Test
    public void should_factor_dependency_before_inject() {
        MicroController controller = new MicroController();

        controller.registerDependencyHandler(new DependencyHandlerImpl());

        TestActionClass actionClass = new TestActionClass();
        controller.registerActionClass(actionClass);

        controller.run("test_handled_dependency");

        assertTrue(actionClass.ran);
    }

    @Test
    public void should_dispose_dependency_after_use() {
        MicroController controller = new MicroController();

        controller.registerDependencyHandler(new DependencyHandlerImpl());

        TestActionClass actionClass = new TestActionClass();
        controller.registerActionClass(actionClass);

        controller.run("test_dispose_dependency");

        assertTrue(actionClass.dependency.disposed);
    }

    @Test
    public void should_handle_exceptions() {
        MicroController controller = new MicroController();

        controller.registerDependencyHandler(new DependencyHandlerImpl());

        TestActionClass actionClass = new TestActionClass();
        controller.registerActionClass(actionClass);

        controller.run("test_exception_handle");

        assertFalse(actionClass.dependency.disposed);
        assertTrue(actionClass.dependency.exceptionHandled);
    }

    @Test
    public void should_reuse_dependencies_from_another_controller_but_not_the_controller_itself() {
        MicroController controller = new MicroController();

        Dependency dependency = new Dependency();
        controller.registerDependency(dependency );
        TestActionClass actionClass = new TestActionClass();
        controller.registerActionClass(actionClass);
        
        MicroController another = new MicroController(controller, "another controller");

        another.run("test_dependency_reuse");
        
        assertTrue(actionClass.ran);
        assertSame(actionClass.dependency, dependency);
        assertSame(actionClass.controller, another);
    }
}