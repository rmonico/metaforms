package br.zero.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ConfigTests {

    private Config config;

    @Before
    public void initialize_config() {
        config = new Config() {
            @Override
            protected void createSwitchs(List<Switch> switchs, SwitchFactory f) {
                switchs.add(f.newStringSwitch("jdbcdriver", "J", "org.sqlite.JDBC"));
                switchs.add(f.newStringSwitch("connectionstring", "C"));
                switchs.add(f.newStringSwitch("tablenamepattern", null, "%"));
                switchs.add(f.newStringSwitch("tabletypes", null, "TABLE"));

                switchs.add(f.newFlagSwitch("help", "h"));
            }
        };
    }

    private void parseArgs(String... args) {
        config.parseCommandLineArguments(args);
    }

    @Test
    public void an_empty_command_line_should_return_default_values_only() {
        parseArgs();

        assertEquals(3, config.keys().keySet().size());

        assertEquals("org.sqlite.JDBC", config.key("jdbcdriver"));
        assertEquals("%", config.key("tablenamepattern"));
        assertEquals("TABLE", config.key("tabletypes"));
    }

    @Test
    public void should_pass_and_get_a_param() {
        parseArgs("--connectionstring", "my connection string");

        assertEquals("my connection string", config.key("connectionstring"));
    }

    @Test
    public void should_pass_and_get_a_param_with_default() {
        parseArgs("--jdbcdriver", "my jdbc driver");

        assertEquals("my jdbc driver", config.key("jdbcdriver"));
    }

    @Test
    public void should_pass_a_flag_param() {
        parseArgs("--help");

        assertNotNull(config.key("help"));
    }

    @Test
    public void should_pass_and_get_with_shortkeys() {
        parseArgs("-J", "my jdbc driver");

        assertEquals("my jdbc driver", config.key("jdbcdriver"));
    }

    @Test
    public void should_add_keys_from_stream() throws IOException {
        StringBuilder sb = new StringBuilder();

        sb.append("first_key=value of first key\n");
        sb.append("second_key=value of second key\n");

        InputStream stream = new ByteArrayInputStream(sb.toString().getBytes(StandardCharsets.UTF_8));

        config.addFromStream(stream);

        stream.close();

        assertEquals("value of first key", config.key("first_key"));
        assertEquals("value of second key", config.key("second_key"));
    }
}
