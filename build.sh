#!/bin/bash

echo Removendo pasta build/...
rm -rf build/
echo
echo

echo Recriando pasta build/...
mkdir -p build/metaforms
echo
echo

echo Compilando fontes....
javac -sourcepath src -d build/metaforms src/br/zero/metaforms/Main.java
echo
echo

echo Adicionando dependências...
pushd .
mkdir -p build/sqlite-jdbc-3.8.10.1
cd build/sqlite-jdbc-3.8.10.1
jar -xf ../../lib/sqlite-jdbc-3.8.10.1.jar
find . -type f -name \*.java -or -name \*.c -delete
popd
echo
echo

echo Criando metaforms.jar...
jar cvfm metaforms.jar META-INF/MANIFEST.MF -C build/metaforms . -C build/sqlite-jdbc-3.8.10.1 .
echo
echo

mv metaforms.jar build/

echo OK!